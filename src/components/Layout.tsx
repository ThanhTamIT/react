import React from 'react';
import SideBar from './SideBar';
import PropTypes from 'prop-types'

interface AuxProps {
    children: React.ReactNode;
}

function Layout({children} :AuxProps ) {
    return (
        <div className="my-app-layout">
            <div className="my-app-layout__header">
                <SideBar></SideBar>
            </div>
            <div className="my-app-layout__main">
                <div className="content">
                    {children}
                </div>
            </div>
        </div>
    );
}

Layout.propTypes = {
    children: PropTypes.element
}

export default Layout;
