import React from 'react';
import Nav from 'react-bootstrap/Nav'
import Logo from 'assets/logo.svg';

function SideBar() {
    return (
        <Nav navbar defaultActiveKey="/home" className="flex-column my-app-nav">
            <div className="my-app-nav__logo">
                <img src={Logo} alt="logo"></img>
            </div>
            <Nav.Link href="/home">Survey Search</Nav.Link>
            <Nav.Link href="survey-delivery">Survey Delivery</Nav.Link>
        </Nav>
    );
}

export default SideBar;
