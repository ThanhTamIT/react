import HomePage from 'pages/Home';
import { LoginPage } from 'pages/login/index';
import { RegisterPage } from 'pages/register/index';
import { ForgotPasswordPage } from 'pages/forgot-passwod/index';
import { ResetPasswordPage } from 'pages/reset-passwod/index';
import { ResetPasswordSuccessPage } from 'pages/reset-passwod-success/index';
import PrivateRoute from './private-route';
import PublicRoute from './public-route';
import { ForgotPasswordSuccessPage } from 'pages/forgot-passwod-success';

export const routes = [
    {
      path: "/login",
      component: LoginPage,
      isPrvate: false
    },
    {
      path: "/register",
      component: RegisterPage,
      isPrvate: false
    },
    {
      path: "/forgot-password",
      component: ForgotPasswordPage,
      isPrvate: false
    },
    {
      path: "/forgot-password-success",
      component: ForgotPasswordSuccessPage,
      isPrvate: false
    },

    {
      path: "/reset-password",
      component: ResetPasswordPage,
      isPrvate: false
    },

    {
      path: "/reset-password-success",
      component: ResetPasswordSuccessPage,
      isPrvate: false
    },

    {
      path: "/",
      component: HomePage,
      isPrvate: true
    }
];


export function Route(route: any) {
  return (
    !route.isPrvate ? <PublicRoute {...route}/> : <PrivateRoute {...route} />
  );
}