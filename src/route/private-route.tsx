import React from "react";
import {
  Route, 
  Redirect,
  RouteProps, 
  RouteComponentProps
} from "react-router-dom";
import { getAuth } from "utils/local-storage";
import Layout from 'components/Layout';

interface PrivateRouteProps extends RouteProps {
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>;
}


function PrivateRoute({component: Component, ...rest} : PrivateRouteProps) {
    return (
        <Route {...rest} render={props => {
            if (!getAuth()) {
                return <Redirect to='/login' />
            }
            return <Layout><Component  {...props} /></Layout>
        }} />
    );
}


export default PrivateRoute;
