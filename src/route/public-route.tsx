import React from "react";
import {
  Route, 
  Redirect,
  RouteProps, 
  RouteComponentProps
} from "react-router-dom";
import { getAuth } from "utils/local-storage";

interface PrivateRouteProps extends RouteProps {
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>;
}


function PublicRoute({ component: Component, ...rest }: PrivateRouteProps) {
    return (
        <Route {...rest} render={props => {
            if (!!getAuth()) {
               
                return <Redirect to={{ pathname: '/', state: { from: props.location } }} />
            }

            return <Component  {...props} />
        }} />
    );
}

export default PublicRoute;