import React from 'react';
import { useForm, Controller } from "react-hook-form";
import ReactDatePicker from "react-datepicker";

import ReactSelect from "react-select";

function HomePage() {
    const { register, control, handleSubmit, watch} = useForm();
    const watchShowAge = watch("type");
    function onSubmitFrom(e: any) {
        console.log(e, 'sdfsdfdsfdsf')
    }
    console.log("watchFields", watchShowAge);

    return (
        <div className="my-app-page">
           <div className="my-app-page__header">
               <h3>Questionnaire answer search</h3>
           </div>
           <div className="row">
           <form name="form" onSubmit={handleSubmit(onSubmitFrom)} className="form col-10 m-auto">
              <div className="row align-items-center">
              <div className="col-2"><label>Survey type</label></div>
              <div className="col-8">
                <Controller
                  name="type"
                  control={control}
                  rules={{ required: true }}
                  render={({field}) => (
                    <ReactSelect
                     {...field}
                      options={[
                        { value: "chocolate", label: "Chocolate" },
                        { value: "strawberry", label: "Strawberry" },
                        { value: "vanilla", label: "Vanilla" }
                      ]}
                    />
                  )}
                />
              </div>
            </div>
              <div className="row align-items-center">
              <div className="col-2"><label>Email</label></div>
              <div className="col-8 d-flex">
                <div className="form-check">
                    <input  {...register("test")}
                      className="form-check-input" type="checkbox" 
                        value="1" id="flexCheckDefault"/>
                    <label className="form-check-label" htmlFor="flexCheckDefault">
                      Yes
                    </label>
                  </div>
                  <div className="form-check ml-5">
                    <input {...register("test")} 
                      className="form-check-input" type="checkbox" value="2" id="flexCheckChecked"/>
                    <label className="form-check-label" htmlFor="flexCheckChecked">
                      No
                    </label>
                  </div>
              </div>
            </div>
              <div className="row align-items-center">
              <div className="col-2"><label>Email</label></div>
              <div className="col-8 d-flex">
              <div className="form-check">
                    <input  {...register("test1")}
                      className="form-check-input" type="checkbox" 
                        value="1" id="flexCheckDefault"/>
                    <label className="form-check-label" htmlFor="flexCheckDefault">
                      Yes
                    </label>
                  </div>
                  <div className="form-check ml-5">
                    <input {...register("test1")} 
                      className="form-check-input" type="checkbox" value="2" id="test"/>
                    <label className="form-check-label" htmlFor="test">
                      No
                    </label>
                  </div>
              </div>
            </div>
            <div className="row align-items-center">
              <div className="col-2"><label>Email</label></div>
              <div className="col-8 d-flex">
                <Controller
                  control={control}
                  name="ReactDatepicker"
                  render={({ field }) => (
                    <ReactDatePicker
                      {...field}
                      className="input"
                      placeholderText="Select date"
                      onChange={(e) => field.onChange(e)}
                      selected={field.value}
                      dateFormat="yyyy/MM/dd"
                    />
                  )}
                />
              </div>
            </div>
            <div className="form-group text-center">
              <button className="btn btn-primary">
                  Register
              </button>
            </div>
          </form>
           </div>
        </div>
    );
}

export default HomePage;
