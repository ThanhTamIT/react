import { Link } from 'react-router-dom';
import Card from 'react-bootstrap/Card';

function ForgotPasswordSuccessPage ()  {
    return (
        
    <div className="my-app__auth">
        <Card>
            <Card.Body className="text-center">
                <h2 className="text-center">Forgot Password</h2>
                <p>
                    We have sent an email to the email address you entered to reset your password. Please check it. <br /> Expiration date is 24 hours.
                </p>
                <Link to="/login" className="btn btn-link">Login</Link>
            </Card.Body>
        </Card>
    </div>
    );
}

export { ForgotPasswordSuccessPage };