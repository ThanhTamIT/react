import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from 'store/reducers';
import { AuthActions } from 'store/actions/auth.action';
import { Regexs } from 'shared/constant/regexs';
import Card from 'react-bootstrap/Card';


function RegisterPage() {
    const { register, handleSubmit, formState: { errors, isDirty } } = useForm();
    const isLoading: boolean = useSelector((state: IRootState) => state.auth.isLoading);
    const dispatch = useDispatch();
    const emailPattern = Regexs.email;
    const maxLength = 30;

    function onRegister(e = {}) {
        if(isLoading) {
            return;
        }

        dispatch(AuthActions.register(e));
    }

    return (
        <div className="my-app__auth">
        <Card>
            <Card.Body>
            <h2 className="text-center">Register</h2>

            <form name="form" onSubmit={handleSubmit(onRegister)}>
                <div className="form-group">
                    <label>First Name</label>
                    <input type="text" autoComplete="off"
                        {...register("first_name", { required: true })} 
                        className={'form-control' + (isDirty && errors.first_name ? ' is-invalid' : '')} />
                </div>

                <div className="form-group">
                    <label>Last Name</label>
                    <input type="text" 
                        autoComplete="off"
                        {...register("last_name", { required: true })} 
                        className={'form-control' + (isDirty && errors.last_name ? ' is-invalid' : '')} />
                </div>

                <div className="form-group">
                    <label>Username</label>
                    <input type="text" autoComplete="off"
                        {...register("username", { required: true })} 
                        className={'form-control' + (isDirty && errors.username ? ' is-invalid' : '')} />
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input type="text" autoComplete="off"
                        {...register("email", { required: true, pattern: emailPattern })} 
                        className={'form-control' + (isDirty && errors.email ? ' is-invalid' : '')} />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" autoComplete="off"
                        maxLength={maxLength}
                        {...register("password", { required: true, minLength: 6, maxLength: 30 })} 
                        className={'form-control' + (isDirty && errors.password ? ' is-invalid' : '')} />
                </div>

                <div className="form-group text-center">
                    <button className="btn btn-primary" disabled={isLoading}>
                        {isLoading && <span className="spinner-border spinner-border-sm mr-1"></span>}
                        Register
                    </button>
                    <Link to="/login" className="btn btn-link">Login</Link>
                </div>
            </form>
            </Card.Body>
            </Card>
        </div>
    );
}

export { RegisterPage };