import { Link } from 'react-router-dom';
import Card from 'react-bootstrap/Card';

function ResetPasswordSuccessPage ()  {
    return (
        
    <div className="my-app__auth">
        <Card>
            <Card.Body className="text-center">
                <h2 className="text-center">Forgot Password</h2>
                <p>
                Password reset is complete.Please log in by tapping the button below.
                </p>
                <Link to="/login" className="btn btn-link">Login</Link>
            </Card.Body>
        </Card>
    </div>
    );
}

export { ResetPasswordSuccessPage };