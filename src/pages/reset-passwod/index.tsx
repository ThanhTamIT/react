import { Link, useHistory, useLocation } from 'react-router-dom';
import queryString from 'query-string'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from "react-hook-form";
import { AuthActions } from 'store/actions/auth.action';
import { AuthState } from 'store/reducers/auth.reducer';
import { IRootState } from 'store/reducers';
import Card from 'react-bootstrap/Card';
import { useEffect, useState } from 'react';

function ResetPasswordPage ()  {
    const history = useHistory();
    const { search } = useLocation();
    const authState: AuthState = useSelector((state: IRootState) => state.auth);
    const [params] = useState(queryString.parse(search));
    const maxLength = 30;
    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors, isDirty } } = useForm();

    useEffect(() => {
        if(search.length) {
            history.replace({
                search: ''
            });
        }
        if(authState.isSuccess) {
            history.push('/reset-password-success');
        }
        
      }, [authState.isSuccess, history, search]);

    function onSubmit(e: any) {
        if(authState.isLoading) {
            return;
        }
        dispatch(AuthActions.resetPassword({...e, ...params}));
    }

    return (
        
    <div className="my-app__auth">
        <Card>
            <Card.Body>
                <h2 className="text-center">Reset Password</h2>
                <form name="form" onSubmit={handleSubmit(onSubmit)}>
                <div className="form-group">
                        <label>Password</label>
                        <input type="password"
                            maxLength={maxLength}
                            {...register("password", { required: true, min: 6 })} 
                            className={'form-control' + (isDirty && errors.password ? ' is-invalid' : '')}/>
                    </div>
                    <div className="form-group">
                        <label>Password confirm</label>
                        <input type="password"
                            maxLength={maxLength}
                            {...register("password_confirmation", { required: true, min: 6 })} 
                            className={'form-control' + (isDirty && errors.password_confirmation ? ' is-invalid' : '')}/>
                    </div>
                    <div className="form-group text-center">
                        <button className="btn btn-primary" disabled={authState.isLoading}>
                            {authState.isLoading && <span className="spinner-border spinner-border-sm mr-1"></span>}
                            Submit
                        </button>
                        <Link to="/login" className="btn btn-link">Login</Link>
                    </div>
                </form>
            </Card.Body>
        </Card>
    </div>
    );
}

export { ResetPasswordPage };