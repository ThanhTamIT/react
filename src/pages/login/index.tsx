import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from "react-hook-form";
import { AuthActions } from 'store/actions/auth.action';
import { AuthState } from 'store/reducers/auth.reducer';
import { IRootState } from 'store/reducers';
import Card from 'react-bootstrap/Card';
import { useEffect } from 'react';

function  LoginPage ()  {
    const history = useHistory();
    const authState: AuthState = useSelector((state: IRootState) => state.auth);
    
    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors, isDirty } } = useForm();
    const maxLength: number = 30;

    useEffect(() => {
        if (authState.isSuccess) { 
          history.push("/home");
        }
      }, [authState.isSuccess, history]);

    function onSubmit(e: any) {
        if(authState.isLoading) {
            return;
        }
        dispatch(AuthActions.login(e));
    }

    return (
        
    <div className="my-app__auth">
        <Card>
            <Card.Body>
                <h2 className="text-center">Login</h2>
                <form name="form" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-group">
                        <label>Email</label>
                        <input {...register("email", { required: true })} 
                            name="email" type="text" className={'form-control' + (isDirty && errors.email ? ' is-invalid' : '')}/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password"
                            maxLength={maxLength}
                            {...register("password", { required: true, min: 6 })} 
                            className={'form-control' + (isDirty && errors.password ? ' is-invalid' : '')}/>
                    </div>
                    <div className="form-group text-center">
                        <button className="btn btn-primary" disabled={authState.isLoading}>
                            {authState.isLoading && <span className="spinner-border spinner-border-sm mr-1"></span>}
                            Login
                        </button>
                        <Link to="/forgot-password" className="btn btn-link">Forgot Password</Link>
                    </div>
                </form>
            </Card.Body>
        </Card>
    </div>
    );
}

export { LoginPage };