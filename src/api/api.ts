import Axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import { handleErrorsMessage } from '../utils/message';
import { authHeader } from 'utils/header';
import { useHistory } from 'react-router-dom';
import { removeAuth } from 'utils/local-storage';

Axios.defaults.baseURL = process.env.REACT_APP_API_URL

export function makeRequest(params = {}): AxiosRequestConfig {
    return {
        params,
        headers: {
            ...authHeader(),
            'Content-Type': 'Application/json',
        },
    };
}

export function handleResponse(response: Promise<AxiosResponse>) {
    return response
        .then((res: AxiosResponse) => {
            return res.data;
        })
        .catch((errors: AxiosError) => {
            const res = errors.response as AxiosResponse;
            const message = handleErrorsMessage(res.data, res.status) as string;

            if (res.status === 401) {
                removeAuth();
                const history = useHistory();
                history.push('/login');
            }

            throw {
                ...res.data,
                status: res.status,
                message: message
            };
        });
}

export default {
    get(path: string, params = {}) {
        const request = makeRequest(params);
        return handleResponse(Axios.get(path, request));
    },
    post(path: string, data = {}, params = {}) {
        const request = makeRequest(params);
        return handleResponse(Axios.post(path, data, request));
    },
    put(path: string, data = {}, params = {}) {
        const request = makeRequest(params);
        return handleResponse(Axios.put(path, data, request));
    },
    delete(path: string, params = {}) {
        const request = makeRequest();
        return handleResponse(Axios.request(
            {
                ...request,
                method: 'delete',
                url: path,
                data: params
            }
        ));
    },
};
