import Api from './api';

export default {
    login(data = {}) {
        return Api.post(`admin/auth/login`, data)
        .then((user: any) => {
            return user.data;
        });
    },
    
    logout() {
        return Api.post(`admin/auth/logout`)
        .then(() => {
            localStorage.removeItem('user');
            localStorage.removeItem('token');
        });
    },
    
    register(user: any) {
        return Api.post(`admin/auth/register`, user);
    },
    
    forgotPassword(data = {}){
        return Api.post('admin/password/forgot', data).then((res) => {
            return res;
        });
    },
    
    resetPassword(data = {}){
        return Api.post('admin/password/reset', data).then((res) => {
            return res;
        });
    },
    
    verifyUpdateEmail(data = {}){
        return Api.put('auth/email', data).then((res) => {
            return res;
        });
    },
      
}


