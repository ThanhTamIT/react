import Api from './api';

export const getSurveyByType = (type: number, opts = {}) => {
  return Api.get(`user/surveys/type/${type}`, opts);
};

export const getSurveys = (opts = {}) => {
  return Api.get(`admin/surveys`, opts);
};

export const surveyUsersCount = (opts = {}) => {
  return Api.post(`admin/survey-users/count`, opts);
};

export const usersCount = (data = {}) => {
  return Api.post(`admin/users/count`, data);
};

export const csvRequest = (opts = {}) => {
  return Api.post(`admin/survey-users/csv-request`, opts);
};

export const csvUserInforRequest = (opts = {}) => {
  return Api.post(`admin/users/csv-request`, opts);
};

export const getCsvStatus = (opts = {}) => {
    return Api.get(`admin/csvs/status`, opts);
};
  
