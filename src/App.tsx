import { BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import { Route, routes } from './route/index';

function App() {
    return (
        <div className="my-app">
            <Container fluid className="p-0">
              <Router>
                  <Switch>
                      
                  {
                    routes.map((route, i) => (
                    <Route key={i} {...route} />
                  ))}
                      <Redirect from="*" to="/" />
                  </Switch>
              </Router>
            </Container>
        </div>
    );
}

export default App;
