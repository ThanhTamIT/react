import { getAuth } from "./local-storage";

export function authHeader() {
    const token = getAuth();

    if (!!token) {
        return { 'Authorization': 'Bearer ' + token };
    } else {
        return {};
    }
}
