export const APP = {
    TOKEN: 'MyAppToken',
    USER: 'MyAppUser'
}

export const getAuth = () => localStorage.getItem(APP.TOKEN);
export const setAuth = (token: string) => localStorage.setItem(APP.TOKEN, token);
export const removeAuth = () => localStorage.removeItem(APP.TOKEN);

export const getUser = () => localStorage.getItem(APP.USER);
export const setUser = (user: string) => localStorage.setItem(APP.USER, user);
export const removeUser = () => localStorage.removeItem(APP.USER);