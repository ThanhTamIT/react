export function handleErrorsMessage(errors: any, status?: number): string | undefined {
    switch (errors.constructor) {
        case String:
            return errors;
        case Object:
            if (status === 503) {
                const message = (errors.errors && !!errors.errors.length) ? errors.errors[0].reason : '';
                return message;
            }
            if (status === 401) {
                return '無効なトークンです';
            }
            if (errors.errors.length > 0) {
                return errors.errors[0].reason;
            }
            if (errors.hasOwnProperty('message')) {
                return errors.message;
            }
            break;
        case Array:
            if (errors.length > 0) {
                return errors[0];
            }
            break;
        default:break;
    }
}
