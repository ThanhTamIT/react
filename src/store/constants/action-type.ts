/**
 * Appends REQUEST async action type
 */

export const REQUEST = (actionType: string) => `${actionType}_REQUESTING`;

/**
 * Appends SUCCESS async action type
 */

export const SUCCESS = (actionType: string) => `${actionType}_SUCCESSED`;

/**
 * Appends FAILURE async action type
 */

export const FAILURE = (actionType: string) => `${actionType}_FAILURED`;
