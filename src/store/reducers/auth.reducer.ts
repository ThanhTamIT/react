import { AUTH } from 'store/constants/auth';
import { REQUEST, SUCCESS, FAILURE } from 'store/constants/action-type';

// let user = JSON.parse(localStorage.getItem('user'));
// const initialState = user ? { loggedIn: true, user } : {};
export interface AuthAction  {
    type: string,
    user?: any
}

export interface AuthState  {
    isLoading: boolean,
    user?: any,
    message?: string;
    isSuccess?: boolean;
}

const authState: AuthState = {
   isLoading: false,
   isSuccess: false,
   user: {},
   message: ''
}

export function auth(state = authState, action: any): AuthState {
    switch (action.type) {
        case REQUEST(AUTH.LOGIN):
        case REQUEST(AUTH.REGISTER):
            return {
                isLoading: true,
            };
        case SUCCESS(AUTH.LOGIN):
        case SUCCESS(AUTH.REGISTER):
            return {
                isLoading: false,
                isSuccess: true
            };
        case FAILURE(AUTH.LOGIN):
        case FAILURE(AUTH.REGISTER):
            return {
                isLoading: false,
                user: {},
                message: action.msg
            };
        case REQUEST(AUTH.LOGOUT):
            return {
                isLoading: true
            }
        case FAILURE(AUTH.LOGOUT):
            return {
                isLoading: false,
                message: action.msg
            }
        case REQUEST(AUTH.FORGOT_PASSWORD):
        case REQUEST(AUTH.RESET_PASSWORD):
            return {
                isLoading: true,
                user: action.data
            };
        case SUCCESS(AUTH.FORGOT_PASSWORD):
        case SUCCESS(AUTH.RESET_PASSWORD):
            return {
                isLoading: false,
                isSuccess: true
            };
        case FAILURE(AUTH.FORGOT_PASSWORD):
        case FAILURE(AUTH.RESET_PASSWORD):
            return {
                isLoading: false,
                message: action.msg
            };
        default:
            return state
    }
}