import { combineReducers } from 'redux';
import { auth, AuthState } from './auth.reducer';
export interface IRootState {
    readonly auth: AuthState;
  }
const rootReducer = combineReducers<IRootState>({
    auth
});
export default rootReducer;
