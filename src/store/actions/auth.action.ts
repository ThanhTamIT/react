import { Dispatch } from 'redux';
import AuthService from 'api/auth';

import { setUser, setAuth, removeAuth, removeUser } from 'utils/local-storage';

import { AUTH } from 'store/constants/auth';
import { REQUEST, SUCCESS, FAILURE } from 'store/constants/action-type';


export const AuthActions = {
    login(data = {}) {
        return async (dispatch: (Dispatch)) => {
            dispatch({type: REQUEST(AUTH.LOGIN), data});
            try {
                const { access_token } = await AuthService.login(data);
                if(access_token) {
                    setAuth(access_token);
                    dispatch({ type: SUCCESS(AUTH.LOGIN)});
                }
            } catch (error) {
                const msg = error.message;
                dispatch({type: FAILURE(AUTH.LOGIN), msg });
            }
        };
    },
    
    logout() {
        return async (dispatch: (Dispatch)) => {
            dispatch({type: REQUEST(AUTH.LOGOUT)});
            try {
                await AuthService.logout()
                removeAuth();
                removeUser();
            } catch (error) {
                const msg = error.message;
                dispatch({type: FAILURE(AUTH.LOGOUT), msg });
            }
        };
    },

    register(data = {}) {
        return async (dispatch: (Dispatch)) => {
            dispatch({type: REQUEST(AUTH.REGISTER), data});
            try {
                const { user, access_token } = await AuthService.register(data);
                if(user) {
                    setAuth(access_token);
                    dispatch({ type: SUCCESS(AUTH.REGISTER), user });
                    setUser(JSON.stringify(user));
                }
            } catch (error) {
                const msg = error.message;
                dispatch({type: FAILURE(AUTH.REGISTER), msg });
            }
        };
    },

    forgotPassword(data = {}) {
        return async (dispatch: (Dispatch)) => {
            dispatch({type: REQUEST(AUTH.FORGOT_PASSWORD)});
            try {
                await AuthService.forgotPassword(data);
                dispatch({type: SUCCESS(AUTH.FORGOT_PASSWORD)});
            } catch (error) {
                const msg = error.message;
                dispatch({type: FAILURE(AUTH.FORGOT_PASSWORD), msg });
            }
        };
    },

    resetPassword(data = {}) {
        return async (dispatch: (Dispatch)) => {
            dispatch({type: REQUEST(AUTH.RESET_PASSWORD)});
            try {
                await AuthService.resetPassword(data);
                dispatch({type: SUCCESS(AUTH.RESET_PASSWORD)});
            } catch (error) {
                const msg = error.message;
                dispatch({type: FAILURE(AUTH.RESET_PASSWORD), msg });
            }
        };
    }
};



